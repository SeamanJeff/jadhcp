# README #

A Dockerfile and a docker-compose.yml file that quickly get a DHCP server running in docker container.

### Why would you want to do this? ###

* Super-lightweight - You don't need a VM to isolate your DHCP server
* Makes it easy for your DHCP server to be portable and replicable

### Setup ###
Make sure __docker__ and __docker-compose__ are ready to use
```
$ docker -v
Docker version 1.12.3, build 6b644ec
$ docker-compose -v
docker-compose version 1.9.0, build 2585387
```
Clone the repository somewhere.
```
~/docker$ git clone git@bitbucket.org:SeamanJeff/jadhcp.git
Cloning into 'jadhcp'...
remote: Counting objects: 5, done.
remote: Compressing objects: 100% (4/4), done.
remote: Total 5 (delta 0), reused 0 (delta 0)
Receiving objects: 100% (5/5), done.
Checking connectivity... done.
~/docker$
```
Create a data directory
```
$ sudo mkdir /usr/local/etc/dhcpd
```
Configure dhcp as desired (in /usr/local/etc/dhcpd/dhcpd.conf)
```
ignore client-updates;
ddns-update-style none;
authoritative;

subnet 172.17.3.0 netmask 255.255.255.0 {

        option routers                  172.17.3.1;
        option domain-name              "sub.mydomain.com";
        option domain-name-servers      172.17.2.120;

        option netbios-node-type        8;

        default-lease-time              10800;
        max-lease-time                  43200;

        pool {
                range   172.17.3.50 172.17.3.99;
                allow unknown-clients;
        }
}
```
Create an empty lease file
```
~/docker/jadhcp$ sudo touch /usr/local/etc/dhcpd/dhcpd.leases
```
Check the environment variable indicating which of the Docker hosts's interfaces to listen on for DHCP requests.
```
~/docker/jadhcp$ grep DHCP_IF= tinydhcp/Dockerfile
ENV DHCP_IF="br0"
```
Run __docker-compose build __
```
:~/docker/jadhcp$ docker-compose build
Building tinydhcp
Step 1 : FROM gliderlabs/alpine:3.4
 ---> 9cfff538e583
Step 2 : MAINTAINER Jeff Dickens <dreamgear@gmail.com>
 ---> Running in f4838ec385c9
 ---> 0c46cf899f3c
Removing intermediate container f4838ec385c9
Step 3 : RUN apk add --update dhcp
 ---> Running in d813b2a7a787
fetch http://alpine.gliderlabs.com/alpine/v3.4/main/x86_64/APKINDEX.tar.gz
fetch http://alpine.gliderlabs.com/alpine/v3.4/community/x86_64/APKINDEX.tar.gz
(1/2) Installing libgcc (5.3.0-r0)
(2/2) Installing dhcp (4.3.4-r2)
Executing dhcp-4.3.4-r2.pre-install
Executing busybox-1.24.2-r12.trigger
OK: 8 MiB in 13 packages
 ---> 76fee498374f
Removing intermediate container d813b2a7a787
Step 4 : ENV DHCP_IF "br0"
 ---> Running in 84dd68b50937
 ---> d0cc9dafd70d
Removing intermediate container 84dd68b50937
Step 5 : CMD /usr/sbin/dhcpd -4 -f -d --no-pid -cf /data/dhcpd.conf -lf /data/dhcpd.leases --no-pid ${DHCP_IF}
 ---> Running in 6aa115635c96
 ---> c0a9ed260854
Removing intermediate container 6aa115635c96
Successfully built c0a9ed260854
:~/docker/jadhcp$

```
Fire it up
```
~/docker/jadhcp$ docker-compose up -d
Creating jadhcp_tinydhcp_1
~/docker/jadhcp$
```
Check the log
```
~/docker/jadhcp$ docker logs --tail=12 jadhcp_tinydhcp_1
Internet Systems Consortium DHCP Server 4.3.4
Copyright 2004-2016 Internet Systems Consortium.
All rights reserved.
For info, please visit https://www.isc.org/software/dhcp/
Config file: /data/dhcpd.conf
Database file: /data/dhcpd.leases
PID file: /var/run/dhcp/dhcpd.pid
Wrote 1 leases to leases file.
Listening on LPF/br0/e0:69:95:e5:a2:9e/172.17.3.0/24
Sending on   LPF/br0/e0:69:95:e5:a2:9e/172.17.3.0/24
Sending on   Socket/fallback/fallback-net
Server starting service.
~/docker/jadhcp$
```